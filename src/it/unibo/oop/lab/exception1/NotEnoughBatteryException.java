package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException{
	private static final long serialVersionUID = 1L;
	private final int x;
    private final int y;
    
	public NotEnoughBatteryException(final int initX, final int initY) {
		super("Can not move to pos(" + initX + ", " + initY + "), out of battery");
        this.x = initX;
        this.y = initY;
	}
	
}
