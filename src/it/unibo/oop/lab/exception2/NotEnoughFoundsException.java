package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends IllegalStateException {

	
	private static final long serialVersionUID = 1L;

	public NotEnoughFoundsException() {
		super("Draw denied: not enough money");
	}

}
