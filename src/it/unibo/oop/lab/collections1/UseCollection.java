package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    static final int START_POPULATION=1000;
    static final int FINISH_POPULATION=2000;
    static final int ELEMS=100000;
    static final int TO_MS=100000;
    public static void main(final String... s) {
    	
    	List <Integer> list = new ArrayList<>(); 
    	
    	for(int i = START_POPULATION; i<FINISH_POPULATION; i++) {
    		list.add(i);
    	}
    	
    	List <Integer> linkedlist = new LinkedList<>(list);
    	
    	int temp = linkedlist.get(linkedlist.size()-1);
    	linkedlist.set(linkedlist.size()-1,linkedlist.get(0));
    	linkedlist.set(0, temp);
    	for(int elem : linkedlist) {
    		System.out.println(elem);
    	}
    	
    	long timelinkedlist =System.nanoTime();
    	
    	for (int i = 1; i <= ELEMS; i++) {
            linkedlist.set(0,i);
        }
        
    	timelinkedlist = System.nanoTime() - timelinkedlist;
    	System.out.println(timelinkedlist / TO_MS);
    	
    	long timelist =System.nanoTime();
    	
    	for (int i = 1; i <= ELEMS; i++) {
            list.set(0,i);
        }
        
    	timelist = System.nanoTime() - timelist;
    	System.out.println(timelist / TO_MS);
    	
    	
    	long timelinkedlist2 = System.nanoTime();
    	for(int i=0; i<1000; i++) {
    		linkedlist.get(linkedlist.size()/2);
    	}
    	timelinkedlist2 = System.nanoTime() - timelinkedlist2;
    	
    	System.out.println(timelinkedlist2 / TO_MS);
    	
    	long timelist2 = System.nanoTime();
    	for(int i=0; i<1000; i++) {
    		list.get(list.size()/2);
    	}
    	timelist2 = System.nanoTime() - timelist2;
    	
    	System.out.println(timelist2 / TO_MS);
    	
    	Map<String, Long> map = new TreeMap<>();
    	
    	map.put("Africa", 1110635000L);
    	map.put("Americas", 972005000L);
    	map.put("Antarticas", 0L);
    	map.put("Asia", 4298723000L);
    	map.put("Europe", 742452000L);
    	map.put("Oceania", 38304000L);
    	long totpopulation = 0;
    	for(long population : map.values()) {
    		totpopulation = totpopulation + ((long)population);
    	}
    	 System.out.println(totpopulation);
    	
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    }
}
